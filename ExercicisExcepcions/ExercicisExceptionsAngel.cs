﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ExercicisExcepcions
{
    class ExercicisExceptionsAngel
    {
        static void Main(string[] args)
        {
            ExercicisExceptionsAngel exercicis = new ExercicisExceptionsAngel();
            exercicis.Menu();

        }
        public void Menu()
        {
            MostrarOpcionsMenu();
            string opcio = DemanarOpcioMenu();
            EscollirOpcioMenu(opcio);
        }
        public void MostrarOpcionsMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                         Exercicis Excepcions Angel Navarrete");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t1.- Divideix o 0");
            Console.WriteLine("\t2.- Pasar a Double o 1.0");
            Console.WriteLine("\t3.- Llegir Fitxer");
            Console.WriteLine("\t4.- Pokedex");
            Console.WriteLine("\t0.- Sortir");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public string DemanarOpcioMenu()
        {
            Console.Write("Escull una opció: ");
            string opcio = Console.ReadLine();
            return opcio;
        }

        public void EscollirOpcioMenu(string opcio)
        {
            do
            {
                switch (opcio)
                {
                    case "1":
                        ProvesDivideixoCero();
                        break;
                    case "2":
                        ProvesADoubleoU();
                        break;
                    case "3":
                        ProvesLlegirDocument();
                        break;
                    case "4":
                        ProvesModificarlista();
                        break;
                    case "0":
                        Console.WriteLine("Adeu");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opcio Incorrecta");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
                Console.ReadLine();
                Console.Clear();
                MostrarOpcionsMenu();
                opcio = DemanarOpcioMenu();
            } while (opcio != "0");
        }

        public void ProvesDivideixoCero()
        {
            static int DivideixoCero(int num1, int num2)
            {
                int resultat = 0;
                try
                {
                    resultat = num1 / num2;
                    return resultat;
                }catch(ArithmeticException)
                {
                    return resultat;
                }
            }
            Console.WriteLine("Divisio de 7 i 2: "+ DivideixoCero(7, 2));
            Console.WriteLine("Divisio de 8 i 4: "+ DivideixoCero(8, 4));
            Console.WriteLine("Divisio de 5 i 0: "+ DivideixoCero(5, 0));
        }
        public void ProvesADoubleoU()
        {
            static double ADoubleoU(object num)
            {
                try
                {
                    return Convert.ToDouble(num);
                }
                catch(FormatException)
                {
                    return 1.0; 
                }
            }
            Console.WriteLine("Convertint 7.1 a Double: "+ADoubleoU(7.1));
            Console.WriteLine("Convertint 9. a Double: "+ADoubleoU(9));
            Console.WriteLine("Convertint .2 a Double: "+ADoubleoU(.2));
            Console.WriteLine("Convertint tres a Double: "+ADoubleoU("tres"));
        }
        public void ProvesLlegirDocument()
        {
            static void LlegirDocument(string fitxer)
            {
                try
                {
                    StreamReader sr = File.OpenText(fitxer);
                    string text = sr.ReadToEnd();
                    Console.WriteLine(text);
                    sr.Close();
                }
                catch(IOException e)
                {
                    Console.WriteLine("S'ha produït un error d'entrada / sortida: "+e.Message);
                }
            }
            LlegirDocument("paco.txt");
        }
        class Pokemon
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            

            public Pokemon(int id, string nombre)
            {
                if (nombre is null)
                {
                    throw new System.ArgumentNullException("Nombre no puede ser null");
                }
                else
                {
                    this.Id = id;
                    this.Nombre = nombre;
                }
                        
            }

        }
        public void ProvesModificarlista()
        {
            List<Pokemon> pokedex = new List<Pokemon>();

            static void ModificarLista(List<Pokemon> lista, string accio, Pokemon element)
            {
                if (accio.ToUpper() == "AÑADIR")
                {
                    try
                    {
                        
                        lista.Add(element);
                        Console.WriteLine(element.Nombre +" añadido");
                        //Error
                        lista[-1] = element;
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        Console.WriteLine("Error: " + e.Message);
                    }
                }
                else if(accio.ToUpper() == "ELIMINAR")
                {
                    try
                    {
                        
                        lista.Remove(element);
                        Console.WriteLine(element.Nombre + " eliminado");
                        
                    }catch(InvalidOperationException e)
                    {
                        Console.WriteLine("Error: "+ e.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Operacio Invalida");
                }
            }

            void MostrarLista(List<Pokemon> lista)
            {
                foreach (var pokemon in lista)
                {
                    Console.WriteLine(pokemon.Id + " " + pokemon.Nombre);
                }
            }

            try
            {
                Pokemon chorizard = new Pokemon(6, "Charizard");
                ModificarLista(pokedex, "añadir", chorizard);
                Pokemon mewtwo = new Pokemon(150, "Mewtwo");
                ModificarLista(pokedex, "eliminar", mewtwo);
            
                Pokemon missigno = new Pokemon(0,null);
                ModificarLista(pokedex, "añadir", missigno);
                
            }
            catch(ArgumentNullException e)
            {
                Console.WriteLine("Error : " + e.Message);
            }
            MostrarLista(pokedex);



        }
    }
}
